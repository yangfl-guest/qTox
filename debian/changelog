qtox (1.17.6-1) unstable; urgency=medium

  * Fix FTBFS with ffmpeg 7.0 (Closes: #1072447)
  * Add nvidia abstraction to AppArmor profile (Closes: #1022250)
  * Hotfix exploit with notifications (Closes: #1054638)
  * Bump Standards-Version to 4.7.0

 -- Yangfl <mmyangfl@gmail.com>  Wed, 28 Aug 2024 14:08:04 +0800

qtox (1.17.6-0.1) unstable; urgency=medium

  * Non-maintainer upload.
  * New upstream release.
    - Compatibility with FFmpeg 5.0 (Closes: #1004815).
  * Exclude unused build dependencies, libglib2.0-dev, libgtk2.0-dev,
    libgdk-pixbuf-2.0-dev (Closes: #967718).

 -- Nicholas Guriev <guriev-ns@ya.ru>  Tue, 28 Jun 2022 21:53:37 +0300

qtox (1.17.4-1) unstable; urgency=medium

  * New upstream release
  * Bump Standards-Version to 4.6.0

 -- Yangfl <mmyangfl@gmail.com>  Tue, 21 Dec 2021 15:55:38 +0800

qtox (1.17.3-1) unstable; urgency=medium

  * New upstream release
  * Add AppArmor profile (Closes: #918138)

 -- Yangfl <mmyangfl@gmail.com>  Sun, 21 Feb 2021 23:14:42 +0800

qtox (1.17.2-1.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Backport upstream changes to fix build with Qt 5.15.
  * Run cmake with -Wno-dev in the autopkgtest (closes: #973540).
  * Fix Lintian duplicate-globbing-patterns error.

 -- Dmitry Shachnev <mitya57@debian.org>  Thu, 05 Nov 2020 15:09:24 +0300

qtox (1.17.2-1) unstable; urgency=medium

  * New upstream release (Closes: #959699)
    + Add AppArmor profile (Closes: #918138)
  * Bump Standards-Version to 4.5.0
  * Bump debhelper compat to 13
  * Add upstream metadata

 -- Yangfl <mmyangfl@gmail.com>  Wed, 06 May 2020 02:53:32 +0800

qtox (1.16.3-2) unstable; urgency=medium

  * Backport ec9d9850 to fix CheckAtomic.cmake
  * Backport 6d0885f3 to fix FTBFS on hurd
  * Fix FTBR by setting QT_RCC_SOURCE_DATE_OVERRIDE
  * Fix autopkgtest
  * Bump Standards-Version to 4.3.0
  * Bump debhelper compat to 12

 -- Yangfl <mmyangfl@gmail.com>  Tue, 29 Jan 2019 16:01:45 +0800

qtox (1.16.3-1) unstable; urgency=medium

  * New upstream release
  * Add upstream signing key
  * Backport CheckAtomic.cmake to detect atomic library
  * Try to fix FTBFS on !linux-any by adding build dep libsndio-dev
  * Bump Standards-Version to 4.2.1
  * Add autopkgtest

 -- Yangfl <mmyangfl@gmail.com>  Thu, 08 Nov 2018 20:35:25 +0800

qtox (1.15.0-1) unstable; urgency=medium

  * Initial release (Closes: #788040)

 -- Yangfl <mmyangfl@gmail.com>  Thu, 31 May 2018 13:06:02 +0800
