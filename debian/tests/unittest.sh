#!/bin/sh
set -e

mkdir build
cd build
cmake -Wno-dev ..
for i in \
    test_contactid test_dbschema test_friendmessagedispatcher \
    test_groupmessagedispatcher test_messageprocessor test_offlinemsgengine \
    test_paths test_posixsignalnotifier test_sessionchatlog test_textformatter \
    test_toxid test_toxstring; do
  make $i
done
make test
cd ..
rm -rf build
